# myTestApplication
Simple Python Django application

### Steps to run application

```
go to Folder with manage.py file and make next commands:
```
pip install -r requirements.txt
python manage.py migrate
python manage.py collectstatic
```
Tests:
```
python manage.py test
```
Run server:
```
python manage.py runserver
```
Check out:
http://localhost:8000/myTestApplication/

