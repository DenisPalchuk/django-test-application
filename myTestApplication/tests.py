from django.contrib.admin.options import ModelAdmin
from django.contrib.admin.sites import AdminSite
from django.test import TestCase
from django.urls import reverse

from .models import Category, Product


def create_category(category_name):
  """
  Create a category with the given `categoryName`
  """
  return Category.objects.create(categoryName=category_name)


def create_product(product_name, product_description, category):
  """
  Create a product with the given `productName`, `productDescription` `categoryId`
  """
  return Product.objects.create(productName=product_name, productDescription=product_description,
                                category=category)


class ModelAdminTest(TestCase):
  def setUp(self):
    self.category = Category.objects.create(
      categoryName='Houses',
    )
    self.site = AdminSite()

  def test_model_admin_str(self):
    ma = ModelAdmin(Category, self.site)
    self.assertEqual(str(ma), 'myTestApplication.ModelAdmin')
    ma = ModelAdmin(Product, self.site)
    self.assertEqual(str(ma), 'myTestApplication.ModelAdmin')


class CategoryModelTests(TestCase):
  def test_category_was_created(self):
    category = Category(categoryName='cars')
    self.assertIs(category.categoryName, 'cars')

  def test_category_string_representation(self):
    category = Category(categoryName="cars")
    self.assertEqual(str(category), category.categoryName)


class CategoryViewTests(TestCase):
  def test_no_categories(self):
    """
    If no categories exist, an appropriate message is displayed.
    """
    response = self.client.get(reverse('myTestApplication:category_list'))
    self.assertEqual(response.status_code, 200)
    self.assertContains(response, "No records here.")
    self.assertQuerysetEqual(list(response.context['categoryList']), [], ordered=False)

  def test_two_categories(self):
    """
    The category_list page may display multiple categories.
    """
    create_category('bikes')
    create_category('cars')
    response = self.client.get(reverse('myTestApplication:category_list'))
    self.assertQuerysetEqual(response.context['categoryList'],
                             ['<Category: bikes>', '<Category: cars>'], ordered=False)


class ProductModelTests(TestCase):
  def test_product_was_created(self):
    category = Category(categoryName='cars')
    product = Product(productName='Honda', productDescription="Accord", categoryId=category)
    self.assertIs(product.productName, 'Honda')
    self.assertIs(product.productDescription, 'Accord')
    self.assertIs(product.categoryId, category)

  def test_product_string_representation(self):
    product = Product(productName="Honda")
    self.assertEqual(str(product), product.productName)

  def test_Product_wasnt_created_without_category(self):
    product = Product(productName='Honda', productDescription="Accord", categoryId=None)
    self.assertIs(product.productName, 'Honda')


class URLTests(TestCase):
  def test_urls_category(self):
    url = reverse('myTestApplication:product_list', args=[1])
    self.assertEqual(url, '/myTestApplication/1/products/')
    url = reverse('myTestApplication:category_list')
    self.assertEqual(url, '/myTestApplication/')
    url = reverse('myTestApplication:category_new', args=[1])
    self.assertEqual(url, '/myTestApplication/category/1/new/')

  def test_urls_products(self):
    url = reverse('myTestApplication:product_list', args=[1])
    self.assertEqual(url, '/myTestApplication/1/products/')
    url = reverse('myTestApplication:product_edit', args=[1, 1])
    self.assertEqual(url, '/myTestApplication/1/products/1/edit/')
    url = reverse('myTestApplication:product_delete', args=[1, 1])
    self.assertEqual(url, '/myTestApplication/1/products/1/delete/')

  def test_urls_exports(self):
    url = reverse('myTestApplication:export_products', args=[1])
    self.assertEqual(url, '/myTestApplication/1/export_products/')
