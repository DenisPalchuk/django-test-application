from django.apps import AppConfig


class MyTestApplicationConfig(AppConfig):
    name = 'myTestApplication'
