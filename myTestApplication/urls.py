from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'myTestApplication'
urlpatterns = [
                url(r'^$', views.category_list, name='category_list'),
                url(r'^(?P<pk>[0-9]+)/export_categories/$', views.export_categories,
                    name='export_categories'),
                url(r'^(?P<pk>[0-9]+)/export_products/$', views.export_products,
                    name='export_products'),
                url(r'^(?P<pk>[0-9]+)/export_total/$', views.export_total, name='export_total'),
                url(r'^category/new/$', views.category_new, name='category_new'),
                url(r'^category/(?P<pk>[0-9]+)/new/$', views.category_new, name='category_new'),
                url(r'^category/(?P<pk>[0-9]+)/edit/$', views.category_edit, name='category_edit'),
                url(r'^category/(?P<pk>[0-9]+)/delete/$', views.category_delete,
                    name='category_delete'),
                url(r'^(?P<pk>[0-9]+)/subcategory/$', views.category_list, name='category_list'),
                url(r'^(?P<pk>[0-9]+)/products/$', views.product_list, name='product_list'),
                url(r'^(?P<categoryId>[0-9]+)/products/new/$', views.product_edit,
                    name='product_edit'),
                url(r'^(?P<categoryId>[0-9]+)/products/(?P<pk>[0-9]+)/edit/$', views.product_edit,
                    name='product_edit'),
                url(r'^(?P<categoryId>[0-9]+)/products/(?P<pk>[0-9]+)/delete/$',
                    views.product_delete, name='product_delete'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
