# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-08 08:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myTestApplication', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='catrgoryId',
            new_name='categoryId',
        ),
    ]
