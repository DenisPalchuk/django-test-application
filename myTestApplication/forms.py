from django import forms
from .models import Category, Product


class CategoryForm(forms.ModelForm):
  class Meta:
    model = Category
    fields = ('categoryName', 'parentId',)


class ProductForm(forms.ModelForm):
  class Meta:
    model = Product
    fields = ('productName', 'productDescription', 'categoryId',)
