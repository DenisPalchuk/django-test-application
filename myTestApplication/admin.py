from django.contrib import admin
from .models import Category, Product


class ProductInline(admin.TabularInline):
  model = Product
  extra = 1


class CategoryAdmin(admin.ModelAdmin):
  fieldsets = [
    (None, {'fields': ['categoryName']}),
    (None, {'fields': ['parentId']})
  ]
  inlines = [ProductInline]
  list_display = ('categoryName', 'parentId')
  search_fields = ['categoryName']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product)
