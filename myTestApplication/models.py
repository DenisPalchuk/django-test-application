from django.db import models


class Category(models.Model):
  categoryName = models.CharField(max_length=200)
  parentId = models.ForeignKey('self', blank=True, null=True)

  def __str__(self):
    return self.categoryName


class Product(models.Model):
  productName = models.CharField(max_length=200)
  productDescription = models.TextField(max_length=200)
  categoryId = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)

  def __str__(self):
    return self.productName
