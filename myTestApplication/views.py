from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse
from django.views import generic
from django.template import loader
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404, render, redirect
from .models import Category, Product
from .forms import CategoryForm, ProductForm
from django.utils import timezone


def category_list(request, pk=None):
  """ display the list of categories """

  categoryList = Category.objects.filter(parentId=pk).order_by('id')
  category = None
  if pk:
    category = get_object_or_404(Category, pk=pk)

  page = request.GET.get('page', 1)
  paginator = Paginator(categoryList, 15)
  try:
    categories = paginator.page(page)
  except PageNotAnInteger:
    categories = paginator.page(1)
  except EmptyPage:
    categories = paginator.page(paginator.num_pages)

  return render(request, 'myTestApplication/category_list.html',
                {'categoryList': categoryList, 'pk': pk, 'category': category,
                 'categories': categories})


def category_new(request, pk=None):
  """ add new category """

  category = None
  if pk:
    category = get_object_or_404(Category, pk=pk)

  if request.method == "POST":
    form = CategoryForm(request.POST)
    if form.is_valid():
      categoryAction = form.save(commit=False)
      categoryAction.save()
      if category is None:
        return redirect('myTestApplication:category_list')
      else:
        return redirect('myTestApplication:category_list', pk=pk)
  else:
    form = CategoryForm(initial={'parentId': pk})

  return render(request, 'myTestApplication/category_edit.html', {'form': form, 'pk': pk})


def category_edit(request, pk=None):
  """ edit existing category """

  category = get_object_or_404(Category, pk=pk)
  parentId = category.parentId
  if request.method == "POST":
    form = CategoryForm(request.POST, instance=category)
    if form.is_valid():
      categoryAction = form.save(commit=False)
      categoryAction.save()
      return redirect('myTestApplication:category_list')
  else:
    form = CategoryForm(instance=category)

  return render(request, 'myTestApplication/category_edit.html',
                {'form': form, 'category': category})


def category_delete(request, pk):
  """ delete existing category """

  category = get_object_or_404(Category, pk=pk)
  category.delete()

  return redirect('myTestApplication:category_list')


def product_list(request, pk):
  """ display the list of products """

  productList = Product.objects.filter(categoryId=pk).order_by('id')
  category = get_object_or_404(Category, pk=pk)

  page = request.GET.get('page', 1)
  paginator = Paginator(productList, 15)
  try:
    products = paginator.page(page)
  except PageNotAnInteger:
    products = paginator.page(1)
  except EmptyPage:
    products = paginator.page(paginator.num_pages)

  return render(request, 'myTestApplication/product_list.html',
                {'productList': productList, 'pk': pk, 'category': category, 'products': products})


def product_edit(request, categoryId, pk=None):
  """ add new product """

  product = None
  if pk:
    product = get_object_or_404(Product, pk=pk)

  if request.method == "POST":
    form = ProductForm(request.POST, instance=product)
    if form.is_valid():
      product = form.save(commit=False)
      product.save()
      return redirect('myTestApplication:product_list', pk=categoryId)
  else:
    form = ProductForm(instance=product, initial={'categoryId': categoryId})

  return render(request, 'myTestApplication/product_edit.html',
                {'form': form, 'pk': pk, 'categoryId': categoryId, 'product': product})


def product_delete(request, categoryId, pk):
  """ delete product """

  product = get_object_or_404(Product, pk=pk)
  product.delete()

  return redirect('myTestApplication:product_list', pk=categoryId)


import csv


def download_csv(request, queryset, exportType=None):
  """
  function to download csv file
  exportType = 'category' to export categories
  exportType = 'product' to export products
  """
  response = HttpResponse(content_type='text/csv')
  response['Content-Disposition'] = 'attachment;filename=export.csv'
  writer = csv.writer(response)

  if exportType == 'category':
    writer.writerow([str(queryset[0]), ])
    writer.writerow([
      '---',
      str(u"ID"),
      str(u"Name"),
      str(u"Category"),
    ])
    print(queryset[1:])
    for obj in queryset[1:]:
      for item in obj:
        writer.writerow([
          '---',
          str(item.id),
          str(item.categoryName),
          str(item),
        ])
  elif exportType == 'product':
    writer.writerow([str(queryset[0]), ])
    writer.writerow([
      '---',
      str(u"Id"),
      str(u"productName"),
      str(u"Description"),
    ])
    for obj in queryset[1:]:
      for item in obj:
        writer.writerow([
          '---',
          str(item.id),
          str(item.productName),
          str(item.productDescription),
        ])
  else:
    writer.writerow(queryset)

  return response


def export_csv_file(request, data, exportType=None):
  data = download_csv(request, data, exportType=exportType)
  return HttpResponse(data, content_type='text/csv')


def export_categories(request, pk):
  """function to export categories"""

  topLevelCategory = Category.objects.get(id=pk)
  subcategory = topLevelCategory.category_set.all()
  data = [topLevelCategory, subcategory]

  return export_csv_file(request, data, exportType='category')


def export_products(request, pk):
  """ function to export products"""

  parentCategory = Category.objects.get(id=pk)
  products = Product.objects.filter(categoryId=pk)
  data = [parentCategory, products]

  return export_csv_file(request, data, exportType='product')


from django.db import connection


def export_total(request, pk):
  """ugly report =("""

  cursor = connection.cursor()
  data = []
  for p in Category.objects.raw('SELECT * FROM ' + Product.objects.model._meta.db_table +
                                ' LEFT JOIN ' + Category.objects.model._meta.db_table +
                                ' ON myTestApplication_product.categoryId_id = myTestApplication_category.id' +
                                ' WHERE myTestApplication_category.parentId_id = ' + pk):
    data.append([p.categoryName, str(p.id), p.productName, p.productDescription])
  return export_csv_file(request, data)
